
import React, { Component, useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import HomeScreen from './screens/Home';
import Liikepankki from './screens/LiikePankki';
import liikHarj from './screens/Liikkuvuusharjoitteet';
import lihasHarj from './screens/Lihaskuntoharjoitteet';
import tulosTaul from './screens/tulosTaul';
import logIn from './screens/logIn';
import register from './screens/register';
// import loading from './screens/loading';
import {Provider as AuthProvider} from './components/context';
import {Context as AuthContext} from './components/context';
import askelMit from './screens/Askelmittari';
import Gps from './screens/Gps'





 

const HomeStack = createStackNavigator();
const AuthStack = createStackNavigator();


const HomeScreens = () => (

  <HomeStack.Navigator initialRouteName="Home">
    <HomeStack.Screen name="Home" component={HomeScreen} options= {{title: 'Kiipula'}}/>
    <HomeStack.Screen name="Liikepankki" component = {Liikepankki} options = {{title: 'Liikepankki'}}/>
    <HomeStack.Screen name="Liikkuvuusharjoitteet" component = {liikHarj}/>
    <HomeStack.Screen name="Lihaskuntoharjoitteet" component = {lihasHarj}/>
    <HomeStack.Screen name="TulosTaulu" component = {tulosTaul} />  
    <Stack.Screen name="GPS" component = {Gps} />
    <Stack.Screen name="Askelmittari" component = {askelMit} options = {{title: 'Askelmittari'}}/>
    {/* <HomeStack.Screen name="logIn" component={logIn}/>
    <HomeStack.Screen name="register" component={register}/>
    <HomeStack.Screen name="loading" component={loading}/> */}

  </HomeStack.Navigator> 
);

const AuthStackScreen = () => (

  <AuthStack.Navigator initialRouteName="logIn" headerMode = "none" >
    <AuthStack.Screen name="logIn" component={logIn}/>
    <AuthStack.Screen name="register" component={register}/>
 
  </AuthStack.Navigator> 
);

const Stack = createStackNavigator();
function App() {
  const {state, _retrieveData} = React.useContext(AuthContext);


  
  _retrieveData;
  
  return (
    <NavigationContainer>
       <Stack.Navigator>
     
        {state.token === null ? (
          <>
            <Stack.Screen
              options={{headerShown: false}}
              name="Auth"
              component={AuthStackScreen}
            />
          </>
        ) : (
          <Stack.Screen
            options={{headerShown: false}}
            name="Home"
            component={HomeScreens}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default () => {
  return (
    <AuthProvider>
      <App />
    </AuthProvider>
  );
};
