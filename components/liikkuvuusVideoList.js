import React, {useRef, useState} from 'react';
import {View, StyleSheet, ActivityIndicator, FlatList} from 'react-native';

import Player from './Player';


const VIDEO_LIST2 = [
    '2IRF8g60ty0',
    'qzRCRfpmKHY',
    's7pTkTDaYKM',
    'fLhVRqOIZ0c',
    'HEifYihChEk',
    'qqpM23QR2Oc',
    '-TDNjbRdfXo',
    'WtSs_ArM7mo',
    'f3q8yYOEEbs',
    'cm-iVmNRZbM',
    'RGXE0OjTuDo',
    '_rWessz_CYs',
    
  ];

const Placeholder = () => {
  return (
    <View style={styles.item}>
      
    </View>
  );
};

const ListVideo = () => {
  const [visablePostIndex, setVisablePostIndex] = useState(0);

  const onViewRef = useRef(({viewableItems}) => {
    if (viewableItems && viewableItems[0]) {
      const index = viewableItems[0].index;
      if (typeof index === 'number') {
        setVisablePostIndex(index);
      }
    } else {
      setVisablePostIndex(-1);
    }
  });
  const viewConfigRef = useRef({viewAreaCoveragePercentThreshold: 80});

  return (
    <View style={styles.container}>
      <FlatList
        data={VIDEO_LIST2}
        viewabilityConfig={viewConfigRef.current}
        onViewableItemsChanged={onViewRef.current}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item, index}) =>
          index === visablePostIndex ? (
            <Player videoId={item} />
          ) : (
            <Placeholder />
          )
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  item: {
    width: '100%',
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gray',
  },
});

export default ListVideo;