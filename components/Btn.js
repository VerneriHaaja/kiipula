import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {Context as AuthContext} from '../components/context';





 const Buttons = () => {

  const onPress = () => Alert.alert('hello mate');
  const navigation = useNavigation(); 
  const {signout} = React.useContext(AuthContext);
  
  const doLogout = () => {

    AsyncStorage.removeItem("token")
    .then(
      res => {
        navigation.navigate('logIn');
        
      }
    )
  
  }

  return(
    
     
      <View style = {styles.btnContainer}> 
        <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('Liikepankki')}
        
      >
         <Text style = {styles.text}>Liikepankki
        <Image source ={require('../icons/icons8-running-24.png')}/>
       </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('Askelmittari')}
      >
        <Text style = {styles.text}>Askelmittari
        <Image source ={require('../icons/icons8-counter-24.png')}/>
       </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('TulosTaulu')}
      >
        <Text style = {styles.text}>Tulostaulu
        <Image source ={require('../icons/icons8-table-24.png')}/>
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('GPS')}
      >
        <Text style = {styles.text}>GPS -"Peli" 
        <Image source ={require('../icons/icons8-treasure-map-24.png')}/>
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btn}
        onPress={onPress}
      >
        <Text style = {styles.text}>Asetukset
        <Image source ={require('../icons/icons8-settings-24.png')}/>
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => signout()}
      >
        <Text style = {styles.text}>Kirjaudu ulos
       
        </Text>
        </TouchableOpacity>
      </View>
     
   
   
  );
};

const styles = StyleSheet.create({
 
  btn: {
    
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 20,
    width: 250,
    borderRadius: 100/2,
    alignSelf: 'center',
  },
  text: {
    fontSize: 20,
    
   
    
  },
  btnContainer: {
    flex: 0.8,
    justifyContent: "space-between",
  },
  
});

export default Buttons;
