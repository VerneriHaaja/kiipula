import createDataContext from './creteDataContext';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import React, {useEffect} from 'react';
import {Value} from 'react-native-reanimated';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'RESTORE_TOKEN':
          return {
            token: action.token, 
          };
       
    case 'signout':
      return {token: null, userName: ''};
    case 'signin':
    case 'signup':
      return {
        token: action.payload.token,
        userName: action.payload.userName,
      };
    default:
      return state;
  }
};

// const signup = dispatch => {
//   return ({userName, password}) => {
//     console.log('Signup');
//   };
// };
const _storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    // Error saving data
  }
};
const _retrieveData = async dispatch => {
  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let token;

      try {
        token = await AsyncStorage.getItem('token');
      } catch (e) {
        // Restoring token failed
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: token });
    };

    bootstrapAsync();
  }, []);
};


const signin = (dispatch) => {
  return ({userName, password}) => {
    if(userName && password){
    const req = {
        "userName": userName,
        "password": password
    }
    axios.post("http://192.168.1.104:3000/api/userModel/login", req)
    .then(
         res =>{ 
                      
            _storeData("token", res.data)
                   // console.warn(res.data);
                    dispatch({
                        type: 'signin',
                        payload: {
                        token: res.data,
                        userName,
                    },
                    });
                       
                    alert("Kirjautuminen onnistui")
               
           
        },
        (err) => {
          alert('Tunnukset ovat väärin');
        },
      );
    } else {
      alert('Anna käyttäjätunnus ja salasana');
    }
  };
};

const signout = (dispatch) => {
  return () => {
    AsyncStorage.removeItem('token')
    dispatch({type: 'signout'});
  };
};

const getToken = (dispatch) => {
  return async() => {
    try {
    token = _retrieveData()
    if (token !== null) {
      const config = {
        method: 'get',
        url: 'http://192.168.1.108:3000/api/userModel/getData',
        headers: { 'Authorization': `Bearer ${token._W}` }
      }
      let res = await axios(config)
      console.log(res.data)
    }
  } catch (error) {
    // Error retrieving data
    console.log("bfer")
  }}
  /*return () => {
    if (token) {
      const config = {
        method: 'get',
        url: 'http://192.168.1.108:3000/api/userModel/getData',
        headers: { 'Authorization': `Bearer ${token._W}` }
      }
      let res = await axios(config)
      console.log(res.data)
      return res.data
    } else {
      return "res.data"
    }
  };*/
};


export const {Provider, Context} = createDataContext(
  authReducer,
  {signin, signout, _retrieveData},
  {token: null, userName: ''},
);
