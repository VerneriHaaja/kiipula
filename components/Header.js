import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({ title }) => {
    return(
        <View style={styles.header}>
            <Text style={styles.text}>{title}</Text>

        </View>
    )
};


Header.defaultProps = {
    title: 'KIIPULA',
};

const styles = StyleSheet.create({
    header: {
        height: 80,
        padding: 20,
        backgroundColor: 'white',
        borderStyle: 'solid',
        borderColor: 'grey',
        borderWidth: 3,
        
    },
    text: {
        color: 'black',
        fontSize: 30,
        textAlign: 'center'
    },
});

export default Header;