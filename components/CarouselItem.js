import React, { PureComponent } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert, Image } from 'react-native';
import * as Animatable from 'react-native-animatable'
import Collapsible from 'react-native-collapsible'


export default class CarouselItem extends PureComponent {

    state = { collapsed : true}

    toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
    this.props.parentCallback(this.state.collapsed);
  };

    render() { 
        return(
        <TouchableOpacity onPress={this.toggleExpanded} style={this.styles.but}>
            <Animatable.View
                duration={300}
                style={[this.styles.cardContainer, this.state.collapsed ? this.styles.active : this.styles.inactive]}
                transition="height"
                >
                <Text style={this.styles.cardTitle}>{this.props.name}</Text>
                <Animatable.Image style={[this.styles.expand , this.state.collapsed ? this.styles.rotateActive : this.styles.rotateInactive]} source={require('../icons/icons8-expand-arrow-24.png')} transition="rotate"/>
                <Collapsible collapsed={this.state.collapsed}>
                <Text style={this.styles.cardInfo}>{this.props.content}</Text>
                </Collapsible>
                <Image style={this.styles.cardImage} source={this.props.image}/>
            </Animatable.View>
        </TouchableOpacity>
        )
    
    }

    styles = StyleSheet.create({
        cardContainer:{
          backgroundColor: 'rgba(0,0,0,0.6)',
          height: 200,
          width: 300,
          bottom:0,
          padding: 24,
          borderRadius: 24,
        },
        but:{
            position:"absolute",
            bottom:0,
          },
        cardImage: {
          height: 120,
          width: 300,
          bottom: 0,
          position: 'absolute',
          borderBottomLeftRadius: 24,
          borderBottomRightRadius: 24,
        },
        cardTitle:{
          color: 'white',
          fontSize: 22,
          alignSelf: 'center',
        },
        cardInfo:{
          marginTop: 10,
          color: 'white',
        },
        expand: {
          position: "absolute",
          top: 25,
          right: 25,
        },
        active: {
          height: 200,
        },
        inactive: {
          height: 400,
        }, 
        rotateActive:{
          transform: [{rotate:"0deg"}]
        },
        rotateInactive:{
          transform: [{rotate:"180deg"}]
        }
      });
    

  }