import React, { Component, useEffect } from 'react';
import { View, Text, StyleSheet, Alert, Platform, Dimensions, Image, TouchableOpacity } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, Callout, AnimatedRegion, Animated } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {request, PERMISSIONS} from 'react-native-permissions';
import Carousel from 'react-native-snap-carousel';
import { ScreenStackHeaderRightView } from 'react-native-screens';
import Collapsible from 'react-native-collapsible'
import * as Animatable from 'react-native-animatable'
import CarouselItem from '../components/CarouselItem'
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

// UserLocation Animation????


export default class Gps extends Component{

  watchId

  state = {
    region:{latitude: 61.016444, longitude: 25.640861, latitudeDelta: 0.015, longitudeDelta: 0.015},
    markers:[],
    coordinates:[
      {name:'Tapahtumapuiston puuveistos',latitude:61.016444,longitude:25.640861, image: require('../images/TapahtumapuistonPuuveistos2.jpg'), content:"Kiikunlähteen turkoosin värinen vesi", found:false},
      {name:'Lahden matkakeskuksen alikulkutunnelin maskit',latitude:60.976361,longitude:25.658972, image: require('../images/AlikulkutunnelinMaskit2.jpg'), content:"some content", found:false},
      {name:'Fellmanninpuiston muistomerkki',latitude:60.984167,longitude:25.644639, image: require('../images/FellmanninpuistonMuistomerkki2.jpg'), content:"some content", found:false},
      {name:'Hakkapeliittapatsas',latitude:60.983,longitude:25.656139, image: require('../images/HakkapeliittaPatsas2.jpg'), content:"some content", found:false},
      {name:'Hiihtäjäpatsas',latitude:60.984361,longitude:25.634556, image: require('../images/HiihtäjäPatsas2.jpg'), content:"some content", found:false},
      {name:'Hyppyrimäet',latitude:60.983778,longitude:25.632694, image: require('../images/Hyppyrimäet2.jpg'), content:"some content", found:false},
      {name:'Häränsilmä',latitude:60.986861,longitude:25.633694, image: require('../images/Häränsilmä2.jpg'), content:"some content", found:false},
      {name:'Kirkkopuiston patsas',latitude:60.984917,longitude:25.659556, image: require('../images/KirkkopuistonPatsas2.jpg'), content:"some content", found:false},
      {name:'Lanupuisto',latitude:60.991028,longitude:25.652889, image: require('../images/LanupuistonPatsas2.jpg'), content:"some content", found:false},
      {name:'Jari Litmasen Patsas',latitude:60.987194,longitude:25.651167, image: require('../images/LitmanenPatsas2.jpg'), content:"some content", found:false},
      {name:'Paasikiven Patsas',latitude:60.983167,longitude:25.650667, image: require('../images/PaasikiviPatsas2.jpg'), content:"some content", found:false},
      {name:'Perhepuiston skeittirampit',latitude:60.969639,longitude:25.649778, image: require('../images/PerhepuistonSkeittirampit2.jpg'), content:"some content", found:false},
      {name:'Radiomastot',latitude:60.978722,longitude:25.649278, image: require('../images/Radiomastot2.jpg'), content:"some content", found:false},
      {name:'Radiomuseon eläimet',latitude:60.979167,longitude:25.646389, image: require('../images/RadiomuseonEläimet2.jpg'), content:"some content", found:false},
      {name:'Tiirismaan lenkkipolku',latitude:60.979417,longitude:25.669861, image: require('../images/PerhepuistonSkeittirampit2.jpg'), content:"some content", found:false},
      {name:'Vesiurut',latitude:60.987444,longitude:25.64575, image: require('../images/Vesiurut2.jpg'), content:"some content", found:false},
    ],
    collapsed: false,
    following: true,
    zoomLevel:0.015
  }

  componentDidMount(){
    this.requestLocationPermission()
    this.fetchData(this)
  }

  componentWillUnmount(){
    Geolocation.clearWatch(this.watchId)
    Geolocation.stopObserving()
  }

  requestLocationPermission = async () => {
    if(Platform.OS === 'ios'){
      var response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
      if(response === 'granted'){
        this.locateCurrentPosition()
      }
    }else{
      var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      if(response === 'granted'){
        this.locateCurrentPosition()
      }
    }
  }

  locateCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
      let currPosition = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: this.state.zoomLevel,
        longitudeDelta: this.state.zoomLevel
      }

      this.setState({region:currPosition})
      this.setState({_mapping:this._map})
      this._map.animateToRegion(this.state.region,1000)
      this.startLocationWatching()
    },
    error => Alert.alert(error.message),
    {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
  );
  }

  startLocationWatching = () =>{
      this.watchId = Geolocation.watchPosition(
        position => {
        let currPosition = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: this.state.zoomLevel,
          longitudeDelta: this.state.zoomLevel
        }
        this.setState({region:currPosition})

        // checking the distance between points and current position and marking found to true if the distance is under 900m
        this.state.coordinates.map(async(element, index) => {
          let d = this.getDistance(element.latitude,element.longitude,this.state.region.latitude,this.state.region.longitude)
          if(d <= 0.1)
          {
            if (this.state.coordinates[index].found != true){
              this.setState(state => {
                return this.state.coordinates[index].found = true;
              })
              // update to database
            }
            try {
              const token = await AsyncStorage.getItem('token');
              if (token !== null) {
                const config = {
                  method: 'put',
                  url: 'http://192.168.1.108:3000/api/userModel/locationFound',
                  headers: { 'Authorization': `Bearer ${token}` },
                  data: {
                    location:this.state.coordinates[index].name
                  }
                }
                let response = await axios(config)
              }
            } catch (error) {
              // Error retrieving data
              console.log(error)
            }
          }
        })

        if(this.state.following){
          this._map.animateToRegion(this.state.region,1000)
        }
      },
      error => Alert.alert(error.message),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
    );
  }

  fetchData = async(thiss) =>{
    let that = thiss
    try {
      const token = await AsyncStorage.getItem('token');
      if (token !== null) {
        const config = {
          method: 'get',
          url: 'http://192.168.1.108:3000/api/userModel/getData',
          headers: { 'Authorization': `Bearer ${token}` }
        }
        let response = await axios(config)
        response.data.locationsFound.forEach(function (location) {
          let elem = that.state.coordinates.find(coords => coords.name === location.name)
          elem.found = location.found
        });
      }
    } catch (error) {
      // Error retrieving data
      console.log(error)
    }
  }

  onCarouselItemChange = (index) => {
    this.setState({ following: false });

    let location = this.state.coordinates[index]
    
    this._map.animateToRegion({
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: this.state.zoomLevel,
          longitudeDelta: this.state.zoomLevel
    },1000)

    this.state.markers[index].showCallout()
  }

  onMarkerPressed = (location,index) => {
    this.setState({ following: false });
    this._map.animateToRegion({
      latitude: location.latitude,
      longitude: location.longitude,
      latitudeDelta: this.state.zoomLevel,
      longitudeDelta: this.state.zoomLevel
    },1000)

    this._carousel.snapToItem(index)
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  onNearMePressed = () =>{
    if(!this.state.following)
    {
      this._map.animateToRegion(this.state.region,1000)
    }

    this.setState({ following: !this.state.following });
  }

  getDistance = (lat1, lon1, lat2, lon2) => {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad = (deg) => {
    return deg * (Math.PI/180)
  }

  handleCallback = (childData) =>{
    this.setState({collapsed: childData})
  }

  renderCarouselItem = ({item}) =>(
    <CarouselItem name={item.name} content={item.content} image={item.image} parentCallback={this.handleCallback}></CarouselItem>
  )
  

  render(){
    return(
      <View style={styles.container}>
      <MapView 
      provider={PROVIDER_GOOGLE}
      ref={map => this._map = map}
      showsUserLocation={true}
      style={styles.map}
      initialRegion={this.state.region}
      showsMyLocationButton={false}
      >
      {
        this.state.coordinates.map((marker, index) => (
          <Marker
          key={ `${marker.name}${Date.now()}` }
          ref={ref => this.state.markers[index] = ref}
          coordinate={{latitude: marker.latitude, longitude: marker.longitude}}
          onPress={() => this.onMarkerPressed(marker, index)}
          title={marker.name}
          pinColor={this.state.coordinates[index].found ? '#376e37' : '#DB322A'}
          >
            <Callout>
              <Text>{marker.name}</Text>
            </Callout>
          </Marker>
        ))
      }
      </MapView>
      
      <TouchableOpacity style={styles.nearMeButton} onPress={() => this.onNearMePressed()}>
        <Image source={require('../icons/icons8-near-me-50.png')}/>
      </TouchableOpacity>
      <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.coordinates}
              containerCustomStyle={[styles.carousel, this.state.collapsed ? styles.collapsed:styles.carousel]}
              renderItem={this.renderCarouselItem}
              sliderWidth={Dimensions.get('window').width}
              itemWidth={300}
              removeClippedSubviews={false}
              onSnapToItem={(index) => this.onCarouselItemChange(index)}
      />
            
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject
    },
    map: {
      ...StyleSheet.absoluteFillObject
    },
    carousel:{
      position: "absolute",
      height:200,
      bottom: 0,
      marginBottom: 12,
    },
    collapsed:{
      height:400,
    },
    nearMeButton:{
      height: 50,
      width: 50,
      position:"absolute",
      top: 12,
      right: 12
    }
  });


