
import React, {useState, useContext} from 'react';
import {Icon} from 'react-native-elements';
import {View, Text, StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import {Input, Button} from 'react-native-elements';
import {Context as AuthContext} from '../components/context';

const Signin = ({navigation}) => {
  const [userName, setuserName] = useState('');
  const [password, setPassword] = useState('');
  const { signin} = useContext(AuthContext);

  return (
    <View style={styles.container}>
    <View style={styles.formWrapper}>
        <Text style={styles.welcomeText}>
            Tervetuloa 
        </Text>
      <View style={styles.formRow}>
          <TextInput
          style={styles.textInput}
           placeholder="Enter username"
           placeholderTextColor="#333"
           value={userName}
           onChangeText={setuserName}/>
      </View>
      <View style={styles.formRow}>
          <TextInput
          style={styles.textInput}
          placeholder="Enter password"
          placeholderTextColor="#333"
          secureTextEntry={true}
          value={password}
           onChangeText={setPassword}/>
      </View>
      <TouchableOpacity 
      onPress={() => signin({userName, password})}
      style={styles.signinBtn}
      >
          <Text style = {styles. signinText}>
              Kirjaudu
          </Text>
      </TouchableOpacity>

    </View>

  </View>
)


};

const styles = StyleSheet.create({
container: {
height: "100%",
alignItems: "center",
justifyContent: "center"
},
formWrapper: {
width: "90%"
},
formRow: {
marginBottom: 10,
},
textInput: {
backgroundColor: "#ddd",
height: 40,
paddingHorizontal: 15,
color: "#333",
},
welcomeText : {
textAlign: "center",
marginBottom: 20,
fontSize: 24,
fontWeight: "bold"
},
signinBtn: {  
alignItems: "center",
backgroundColor: "#DDDDDD",
padding: 10,
width: 200,
borderRadius: 100/2,
alignSelf: 'center',
},
signinText:{
fontWeight: "bold",
fontSize: 18,
}

})


export default Signin;