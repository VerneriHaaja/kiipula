import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';


const Liikepankki = () => {

    const navigation = useNavigation();
    const onPress = () => Alert.alert('hello mate'); 

    return(
        <View style={styles.btnContainer}>
            <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('Liikkuvuusharjoitteet')}>
        <Text style = {styles.text}>Liikkuvuusharjoitteet
        <Image source ={require('../icons/icons8-stretching-24.png')}/>
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('Lihaskuntoharjoitteet')}>
        <Text style = {styles.text}>Lihaskuntoharjoitteet
        <Image source ={require('../icons/icons8-strength-24.png')}/>
        </Text>
      </TouchableOpacity>
        </View>
    );
};


const styles = StyleSheet.create({
 
    btn: {
      
      alignItems: "center",
      backgroundColor: "#DDDDDD",
      padding: 15,
      width: 300,
      borderRadius: 100/2,
      alignSelf: 'center',
    },
    text: {
      fontSize: 20,
    },
    btnContainer: {
      flex: 0.3,
      padding: 50,
      justifyContent: "space-between",
    },
   
  });

export default Liikepankki;