
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';

import Pedometer from 'react-native-pedometer-huangxt';
import BackgroundJob from 'react-native-background-job';

import Button from '../components/Btn'

import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { add } from 'react-native-reanimated';

const JobKey = "JobKey";
const JobKey2 = "JobKey2";

var flag = false;
var prevSteps = 0;
var currSteps = 0;
var addedSteps = 0;

BackgroundJob.register({
  jobKey: JobKey,
  job: () => {
    if(flag === false){
      const now = new Date();
      Pedometer.startPedometerUpdatesFromDate(
        now.getTime(),
        (pedometerData) => {
          // getting the number of steps and printing to console
          currSteps = pedometerData.numberOfSteps;
          console.log(pedometerData.numberOfSteps);
        });
        flag = true;
    }
    }
});

BackgroundJob.register({
  jobKey: JobKey2,
  job: () => {
  addedSteps = currSteps - prevSteps;
  prevSteps = currSteps;
  console.log(addedSteps+ " steps added to database")
  if(addedSteps>=1){fetchData(addedSteps)}
  }
});


BackgroundJob.schedule({
  jobKey: JobKey,
  period: 5000,
  override: false,
  exact: true,
  allowExecutionInForeground: true
});

BackgroundJob.schedule({
  jobKey: JobKey2,
  period: 60000,
  override: false,
  exact: true,
  allowExecutionInForeground: true
});

fetchData = async(addedSteps) =>{
  try {
    const token = await AsyncStorage.getItem('token');
    if (token !== null) {
      const config = {
        method: 'put',
        url: 'http://192.168.1.108:3000/api/userModel/updateSteps',
        headers: { 'Authorization': `Bearer ${token}` },
        data: {
          stepCount:addedSteps
        }
      }
      let response = await axios(config)
    }
  } catch (error) {
    // Error retrieving data
    console.log(error)
  }
}


const Home = () => {
  
  return(
    <View style={styles.container}>
      <Button />
   </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },

});

export default Home;