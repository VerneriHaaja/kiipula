import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Text, View, StyleSheet } from 'react-native';
import { Card } from 'react-native-paper';


function Item({ firstName, lastName }) {
  return (
    <Card style={styles.card}>
      <Text style={styles.paragraph}>{firstName} {lastName}</Text>
    </Card>
  );
}

export default class tulosTaul extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true
    };
  }

  componentDidMount() {
    fetch('http://192.168.1.104:3000/api/userModel/allUsers')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson
        })
      })
      .catch(function(error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
         // ADD THIS THROW error
          throw error;
        });
  }
  _renderItem = ({item, index}) => {
    return(
      <View>
        <Text>
          {item.firstName} {item.lastName}
        </Text>
      </View>
    )
  }

  render() {
    const { data, isLoading } = this.state;

   
    //  if(isLoading){
       return(
    //      <View>
    //       <ActivityIndicator size="large" animating/>
    //      </View>
    //    )

    //  }
    //  else{
    //    return(
    //     <View style={{ flex: 1, padding: 24 }}>
       
    //       <FlatList
    //         data={data}
    //         renderItem={this._renderItem}
    //         keyExtractor={(item, index) => index.toString()}
    //       />
        
    //     </View>
       
      <View style={styles.container}>
          
          
      {this.state.data ? (
        <>
      <Text style={styles.Teksti}>Askeleet</Text>
    <FlatList
      data={this.state.data}
      renderItem={({ item, index }) => <Item firstName={item.firstName} lastName={item.lastName} />}
      keyExtractor={(item, index) => index.toString()}
      
    ></FlatList>
    <Text style={styles.Teksti}>pisteet</Text>
      <FlatList
        data={this.state.data}
        renderItem={({ item,index }) => <Item firstName={item.firstName} lastName={item.lastName} />}
        keyExtractor={(item, index) => index.toString()}
        
      ></FlatList>
      
      </>
   
    
  ) : (
    <Text>Loading…</Text>
  )}
   
  </View>
  )
      
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    padding: 6,
    
  },
  paragraph: {
      margin: 16,
      fontSize: 18,
      // fontWeight: 'bold',
      // textAlign: 'center',
    },
    card: {
      marginBottom: 10
    },
    Teksti: {
      margin: 16,
      fontSize: 18,
       fontWeight: 'bold',
       textAlign: 'center',
    }

});
