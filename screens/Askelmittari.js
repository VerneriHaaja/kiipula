import React , { Component } from 'react';
import { ActivityIndicator, FlatList, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import * as Progress from 'react-native-progress';
import {Surface, Shape} from '@react-native-community/art';
import {Context as AuthContext} from '../components/context';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

export default class askelMit extends Component{

  serverIp = "192.168.1.108"
  port = "3000"
  
  state = { data: {}
  };

  fetchData = async() =>{
    try {
      const token = await AsyncStorage.getItem('token');
      if (token !== null) {
        const config = {
          method: 'get',
          url: 'http://192.168.1.108:3000/api/userModel/getData',
          headers: { 'Authorization': `Bearer ${token}` }
        }
        let response = await axios(config)
        this.setState({data:response.data})
      }
    } catch (error) {
      // Error retrieving data
      console.log(error)
    }
  }

  componentDidMount(){
    this.fetchData()
  }

  render(){
    return (
      <View style={{ flex: 1, padding: 24 }}>
        <Progress.Circle
          style={styles.progress}
          progress={([this.state.data.steps ? this.state.data.steps:0]/3000)}
          size={200}
          thickness={5}
          showsText={true}
          color={'rgb(50, 168, 82)'}
          animated={true}
          borderWidth={1}
          borderColor={'rgb(102, 102, 102)'}/>
        <Text style = {styles.data}>{this.state.data.steps} / 3000 Askelta</Text>
      </View>
    );
  }
  
}

const styles = StyleSheet.create({
 
    btn: {
      
      alignItems: "center",
      backgroundColor: "#DDDDDD",
      padding: 15,
      width: 300,
      borderRadius: 100/2,
      alignSelf: 'center',
    },
    text: {
      fontSize: 20,
    },
    btnContainer: {
      flex: 0.3,
      padding: 50,
      justifyContent: "space-between",
    },
    data:{
        fontSize:20,
        alignSelf:"center"
    },
    progress: {
      margin: 10,
      alignSelf: 'center',
    }
   
});
